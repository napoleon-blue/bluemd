/**
 * Created by tottokotkd on 17/06/05.
 */

import * as _Background from "./background";
import {Chapter} from "./chapter";
import * as PEG from "./peg";
import {IPersonRegister} from "./peg";
import * as _Person from "./person";
import * as _Scene from "./scene";
import * as _Speaker from "./speaker";
import * as _Text from "./text";

const BackgroundRegister = _Background.BackgroundRegister;
const PersonRegister = _Person.PersonRegister;
const getPersonPosition = _Person.getPersonPosition;
const getSceneRegisterMode = _Scene.getSceneRegisterMode;
const Text = _Text.Text;
const Speaker = _Speaker.Speaker;

export function parse(input: string): Chapter[] {
  const commentRemovedInput = PEG.parseComment(input);
  const chapterStrings = PEG.parseChapters(commentRemovedInput);
  return chapterStrings.map((str) => {
    const parsed = PEG.parseChapterContent(str);
    const chapter = new Chapter();
    chapter.title = parsed.title;
    chapter.level = parsed.level;
    chapter.contents = parsed.contents.map((content) => {
      switch (content.type) {
        case "scene": {
          const scenes = parseScene(content.value);
          return {scenes};
        }
        case "text":
          const text = parseText(content.value);
          return {scenes: [], text};

        default:
          return {scenes: []};
      }
    });

    return chapter;
  });
}

export function parseText(input: string): _Text.Text {
  const {title, speaker, values} = PEG.parseText(input);
  const result = new Text();
  result.title = title;
  if (speaker) {
    const spk = new Speaker(speaker.name, speaker.desc);
    return Text.withSpeaker(spk, title, values);
  } else {
    return Text.withTitle(title, values);
  }
}

export function parseScene(input: string): _Scene.SceneRegister[] {
  return PEG.parseScene(input).map((obj) => {
    const {name, type, mode} = obj;
    const modeObj = getSceneRegisterMode(mode);
    switch (type) {
      case "person": {
        const {position, desc} = obj as IPersonRegister;
        const positionObj = getPersonPosition(position);
        const speaker = new Speaker(name, desc);
        return new PersonRegister(speaker, modeObj, positionObj);
      }

      case "background": {
        return new BackgroundRegister(name, modeObj);
      }

      default:
        throw obj;
    }
  });
}

export default {
  parse,
  parseScene,
  parseText,
};
