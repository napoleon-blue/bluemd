/*
 * Text Parser
 * ==========================

* input: string
* output: Text object

## example 1

```
比奈 #困惑 ええ…
無理でスよ。無理っス。
(困った…)
```

{
  title: "比奈",
  speaker: {
    name: "比奈",
    desc: ["困惑"],
  },
  values: [
    "ええ…",
    "無理でスよ。無理っス。",
    "(困った…)",
  ]
}

## example 2

```
:"KR  前回のあらすじ"　途方に暮れるヨンジュと練習生仲間たちの前に忽然と現れた猫おじさん。
```

{
   "title": "KR  前回のあらすじ",
   "speaker": undefined,
   "values": [
      "途方に暮れるヨンジュと練習生仲間たちの前に忽然と現れた猫おじさん。"
   ]
}

## exapmle 3

```
比奈 :ｱﾗｷ　忽然と現れた猫おじさん…？
```

{
   "title": "ｱﾗｷ",
   "speaker": {
      "name": "比奈",
      "desc": []
   },
   "values": [
      "忽然と現れた猫おじさん…？"
   ]
}

*/

Text
  = header:Header _ NewLine? values:Values { return {title: header.title, speaker: header.speaker, values: values}}

Header
  = title:Title { return {title} }
  / Speaker

Speaker
  = name:Name _ title:Title? _ desc:Desc*  { return { title: title || name, speaker: {name, desc}} }

Title
  = ':' title:HeaderString { return title }

Name
  = name:HeaderString { return name }

Desc
  = '#' desc:HeaderString _ { return desc }

HeaderString
  = str:EscapedValue
  / str:[^\r\n:#　 ]+ { return str.join('') }

Values
  = values:(Value NewLine?)* { return values.map(v => v[0]) }

Value
  = text:ValueString { return text }

ValueString
  = str:[^\r\n]+ { return str.join('') }

NewLine
  = [\r?\n]

_
  = [　 ]*

Space
  = [　 ]


// https://stackoverflow.com/questions/33947960/allowing-for-quotes-and-unicode-in-peg-js-grammar-definitions
EscapedValue
  = '"' chars:DoubleStringCharacter* '"' { return chars.join(''); }
  / "'" chars:SingleStringCharacter* "'" { return chars.join(''); }

DoubleStringCharacter
  = !('"' / "\\") char:. { return char; }
  / "\\" sequence:EscapeSequence { return sequence; }

SingleStringCharacter
  = !("'" / "\\") char:. { return char; }
  / "\\" sequence:EscapeSequence { return sequence; }

EscapeSequence
  = "'"
  / '"'
  / "\\"
  / "b"  { return "\b";   }
  / "f"  { return "\f";   }
  / "n"  { return "\n";   }
  / "r"  { return "\r";   }
  / "t"  { return "\t";   }
  / "v"  { return "\x0B"; }
