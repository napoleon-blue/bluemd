/**
 * Created by tottokotkd on 17/06/05.
 */

import {SceneRegister, SceneRegisterMode} from "./scene";
import Speaker from "./speaker";

export type PersonPosition = "left" | "right" | "auto";

export function  getPersonPosition(str: string): PersonPosition {
  switch (str) {
    case "left":
    case "right":
    case "auto":
      return str;
    default :
      return "auto";
  }
}

export class PersonRegister implements SceneRegister {
  public speaker: Speaker;
  public mode: SceneRegisterMode;
  public position: PersonPosition;
  constructor(
    speaker: Speaker,
    mode: SceneRegisterMode = "auto",
    position: PersonPosition = "auto",
  ) {
    this.speaker = speaker;
    this.mode = mode;
    this.position = position;
  }
}

export default PersonRegister;
