/**
 * Created by tottokotkd on 17/06/05.
 */

export class SceneRegister {
  public mode: SceneRegisterMode;
}

export type SceneRegisterMode = "add" | "remove" | "manual" | "auto";

export function  getSceneRegisterMode(str: string): SceneRegisterMode {
  switch (str) {
    case "add":
    case "remove":
    case "manual":
    case "auto":
      return str;
    default :
      return "auto";
  }
}

export default SceneRegister;
