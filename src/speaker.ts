/**
 * Created by tottokotkd on 17/06/05.
 */

export class Speaker {
  public name: string;
  public desc: string[];
  constructor(name: string, desc: string[] = []) {
    this.name = name;
    this.desc = desc;
  }
}

export default Speaker;
